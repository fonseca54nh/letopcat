SOURCES = $(wildcard src/*.md)
OBJS    = $(patsubst %.md,%.html,$(SOURCES))

PRAYERS = $(wildcard src/prayers/*.md)
OBJSCAT = $(patsubst %.md,%.html,$(PRAYERS))

all: $(OBJS) $(OBJSCAT)
	mv src/*.html public
	mv src/prayers/*.html public
	rm -r src/*.tc
	rm -r src/prayers/*.tc
	make server

$(OBJS): %.html: %.md
	#pandoc --css=src/style.css --embed-resource --standalone --metadata title="$@" $< -o $@
	./scripts/preprocess.lua $<
	pandoc --css=src/style.css --embed-resource --standalone -f markdown $<.tc -o $@

$(OBJSCAT): %.html: %.md
	./scripts/preprocess.lua $<
	pandoc --css=src/style.css --embed-resource --standalone -f markdown $<.tc -o $@


clean:
	rm -f src/*.tc
	rm $(OBJS)

server:
	http-server .

killServer:
	pkill http-server &
