<center>
# Angelus Domini
</center>

Angelus Dómini nuntiávit Mariæ.
Et concépit de Spíritu Sancto.

[Ave Maria](aveMaria.html)

Ecce ancílla Dómini.
Fiat mihi secúndum verbum tuum.

[Ave Maria](aveMaria.html)

Et Verbum caro factum est.
Et habitávit in nobis.

[Ave Maria](aveMaria.html)

Ora pro nobis, sancta Dei génetrix.
Ut digni efficiámur promissiónibus Christi.

Orémus:

Grátiam tuam, quǽsumus, Dómine,
méntibus nostris infunde;
ut qui, Ángelo nuntiánte, Christi Fílii tui incarnatiónem cognóvimus, per passiónem eius et crucem, ad resurrectiónis glóriam perducámur. Per eúndem Christum Dóminum nostrum.

Amen.

[Gloria Patri (3x)](gloriaPatri.html)

Requiem aeternam...

Benedictio Apostolica seu Papalis

Dominus vobiscum. Et cum spiritu tuo.
Sit nomen Benedicat vos omnipotens Deus,
Pa ter, et Fi lius, et Spiritus Sanctus.

Amen.
