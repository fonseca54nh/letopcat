[Terço Do Combate](tercoDoCombate.html){clas="link"}

# Pequeno Exorcismo de São Miguel

Sancte Michael Archangele defende nos in proelio
contra niquitias et insidias diaboli este praesidium
imperet illi Deus, supplices deprecamur
Tu que princeps militiae caelestis
satanan aliosque spiritus malignos 
qui ad perditionem animarum pervagantur in mundo
divina virtute in infernum detrude. Amem.
