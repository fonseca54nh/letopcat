[Original Post](https://padrepauloricardo.org/blog/ladainha-do-sagrado-coracao-de-jesus){class="link"}

# Ladainha Do Sagrado Coração De Jesus

Senhor, tende piedade de nós.

Jesus Cristo, tende piedade de nós.

Senhor, tende piedade de nós.

Jesus Cristo, ouvi-nos.

Jesus Cristo, atendei-nos.

Pai celeste, que sois Deus, tende piedade de nós.

Filho, Redentor do mundo, que sois Deus, tende piedade de nós.

Espírito Santo, que sois Deus, tende piedade de nós.

Santíssima Trindade, que sois um só Deus, tende piedade de nós.

Coração de Jesus, Filho do Pai eterno, tende piedade de nós.

Coração de Jesus, formado pelo Espírito Santo no seio da Virgem Mãe, tende piedade de nós.

Coração de Jesus, unido substancialmente ao Verbo de Deus, tende piedade de nós.

Coração de Jesus, de majestade infinita, tende piedade de nós.

Coração de Jesus, templo santo de Deus, tende piedade de nós.

Coração de Jesus, tabernáculo do Altíssimo, tende piedade de nós.

Coração de Jesus, casa de Deus e porta do Céu, tende piedade de nós.

Coração de Jesus, fornalha ardente de caridade, tende piedade de nós.

Coração de Jesus, receptáculo de justiça e de amor, tende piedade de nós.

Coração de Jesus, cheio de bondade e de amor, tende piedade de nós.

Coração de Jesus, abismo de todas as virtudes, tende piedade de nós.

Coração de Jesus, digníssimo de todo o louvor, tende piedade de nós.

Coração de Jesus, Rei e centro de todos os corações, tende piedade de nós.

Coração de Jesus, no qual estão todos os tesouros da sabedoria e ciência, tende piedade de nós.

Coração de Jesus, no qual habita toda a plenitude da divindade, tende piedade de nós.

Coração de Jesus, no qual o Pai põe todas as suas complacências, tende piedade de nós.

Coração de Jesus, de cuja plenitude todos nós participamos, tende piedade de nós.

Coração de Jesus, desejado das colinas eternas, tende piedade de nós.

Coração de Jesus, paciente e de muita misericórdia, tende piedade de nós.

Coração de Jesus, rico para todos que vos invocam, tende piedade de nós.

Coração de Jesus, fonte de vida e santidade, tende piedade de nós.

Coração de Jesus, propiciação por nossos pecados, tende piedade de nós.

Coração de Jesus, saturado de opróbrios, tende piedade de nós.

Coração de Jesus, esmagado de dor por causa dos nossos pecados, tende piedade de nós.

Coração de Jesus, feito obediente até a morte, tende piedade de nós.

Coração de Jesus, transpassado pela lança, tende piedade de nós.

Coração de Jesus, fonte de toda consolação, tende piedade de nós.

Coração de Jesus, nossa vida e ressurreição, tende piedade de nós.

Coração de Jesus, nossa paz e reconciliação, tende piedade de nós.

Coração de Jesus, vítima dos pecadores, tende piedade de nós.

Coração de Jesus, salvação dos que em vós esperam, tende piedade de nós.

Coração de Jesus, esperança dos que morrem em vós, tende piedade de nós.

Coração de Jesus, delícias de todos os santos, tende piedade de nós.

Cordeiro de Deus, que tirais os pecados do mundo, perdoai-nos, Senhor.

Cordeiro de Deus, que tirais os pecados do mundo, ouvi-nos, Senhor.

Cordeiro de Deus, que tirais os pecados do mundo, tende piedade de nós, Senhor.


V. Jesus, manso e humilde de coração,

R. Fazei o nosso coração semelhante ao vosso.

Oremos:

Deus eterno e todo-poderoso, olhai para o Coração do vosso diletíssimo Filho e para os louvores e satisfações que Ele, em nome dos pecadores, vos tem tributado; e, deixando-vos aplacar, perdoai aos que imploram a vossa misericórdia, em nome de vosso mesmo Filho, Jesus Cristo, que convosco vive e reina na unidade do Espírito Santo. Amém.

## Latin

Kyrie, eleison R. Kyrie, eleison. 	

Christe, eleison R. Christe, eleison. 	

Kyrie, eleison R. Kyrie, eleison.

Christe, audi nos R. Christe, audi nos.

Christe, exaudi nos. R. Christe, exaudi nos.

Pater de caelis, Deus, R. miserere nobis.

Fili, Redemptor mundi, Deus, R. miserere nobis.

Spiritus Sancte, Deus, R. miserere nobis.

Sancta Trinitas, unus Deus, R. miserere nobis.

Cor Iesu, Filii Patris aeterni, R. miserere nobis.

Cor Iesu, in sinu Virginis Matris a Spiritu Sancto formatum, R. miserere nobis.

Cor Iesu, Verbo Dei substantialiter unitum, R. miserere nobis.

Cor Iesu, maiestatis infinitae, R. miserere nobis.

Cor Iesu, templum Dei sanctum, R. miserere nobis.

Cor Iesu, tabernaculum Altissimi, R. miserere nobis.

Cor Iesu, domus Dei et porta caeli, R. miserere nobis.

Cor Iesu, fornax ardens caritatis, R. miserere nobis.

Cor Iesu, iustitiae et amoris receptaculum, R. miserere nobis.

Cor Iesu, bonitate et amore plenum, R. miserere nobis.

Cor Iesu, virtutum omnium abyssus, R. miserere nobis.

Cor Iesu, omni laude dignissimum, R. miserere nobis.

Cor Iesu, rex et centrum omnium cordium, R. miserere nobis.

Cor Iesu, in quo sunt omnes thesauri sapientiae et scientiae, R. miserere nobis.

Cor Iesu, in quo habitat omnis plenitudo divinitatis, R. miserere nobis.

Cor Iesu, in quo Pater sibi bene complacuit, R. miserere nobis.

Cor Iesu, de cuius plenitudine omnes nos accepimus, R. miserere nobis.

Cor Iesu, desiderium collium aeternorum, R. miserere nobis.

Cor Iesu, patiens et multae misericordiae, R. miserere nobis.

Cor Iesu, dives in omnes qui invocant te, R. miserere nobis.

Cor Iesu, fons vitae et sanctitatis, R. miserere nobis.

Cor Iesu, propitiatio pro peccatis nostris, R. miserere nobis.

Cor Iesu, saturatum opprobriis, R. miserere nobis.

Cor Iesu, attritum propter scelera nostra, R. miserere nobis.

Cor Iesu, usque ad mortem oboediens factum, R. miserere nobis.

Cor Iesu, lancea perforatum, R. miserere nobis.

Cor Iesu, fons totius consolationis, R. miserere nobis.

Cor Iesu, vita et resurrectio nostra, R. miserere nobis.

Cor Iesu, pax et reconciliatio nostra, R. miserere nobis.

Cor Iesu, victima peccatorum, R. miserere nobis.

Cor Iesu, salus in te sperantium, R. miserere nobis.

Cor Iesu, spes in te morientium, R. miserere nobis.

Cor Iesu, deliciae Sanctorum omnium, R. miserere nobis.

Agnus Dei, qui tollis peccata mundi, R. parce nobis, Domine.

Agnus Dei, qui tollis peccata mundi, R. exaudi nos, Domine.

Agnus Dei, qui tollis peccata mundi, R. miserere nobis, Domine.

V. Iesu, mitis et humilis Corde, R. Fac cor nostrum secundum Cor tuum.

Oremus;

Omnipotens sempiterne Deus, respice in Cor dilectissimi Filii tui et in laudes et satisfactiones, quas in nomine peccatorum tibi persolvit, iisque misericordiam tuam petentibus, tu veniam concede placatus in nomine eiusdem Filii tui Iesu Christi: Qui tecum vivit et regnat in saecula saeculorum. R. Amen. 	

## English

Lord, have mercy R. Lord, have mercy.

Christ, have mercy R. Christ, have mercy.

Lord, have mercy R. Lord, have mercy.

Christ, hear us R. Christ, hear us.

Christ, graciously hear us. R. Christ, graciously hear us.

God the Father of Heaven, R. have mercy on us.

God the Son, Redeemer of the world, R. have mercy on us.

God, the Holy Spirit, R. have mercy on us.

Holy Trinity, One God, R. have mercy on us.

Heart of Jesus, Son of the Eternal Father, R. have mercy on us.

Heart of Jesus, formed by the Holy Spirit in the womb of the Virgin Mother, R. have mercy on us.

Heart of Jesus, substantially united to the Word of God, R. have mercy on us.

Heart of Jesus, of Infinite Majesty, R. have mercy on us.

Heart of Jesus, Sacred Temple of God, R. have mercy on us.

Heart of Jesus, Tabernacle of the Most High, R. have mercy on us.

Heart of Jesus, House of God and Gate of Heaven, R. have mercy on us.

Heart of Jesus, burning furnace of charity, R. have mercy on us.

Heart of Jesus, abode of justice and love, R. have mercy on us.

Heart of Jesus, full of goodness and love, R. have mercy on us.

Heart of Jesus, abyss of all virtues, R. have mercy on us.

Heart of Jesus, most worthy of all praise, R. have mercy on us.

Heart of Jesus, king and center of all hearts, R. have mercy on us.

Heart of Jesus, in whom are all treasures of wisdom and knowledge, R. have mercy on us.

Heart of Jesus, in whom dwelleth the fullness of divinity, R. have mercy on us.

Heart of Jesus, in whom the Father was well pleased, R. have mercy on us.

Heart of Jesus, of whose fullness we have all received, R. have mercy on us.

Heart of Jesus, desire of the everlasting hills, R. have mercy on us.

Heart of Jesus, patient and most merciful, R. have mercy on us.

Heart of Jesus, enriching all who invoke Thee, R. have mercy on us.

Heart of Jesus, fountain of life and holiness, R. have mercy on us.

Heart of Jesus, propitiation for our sins, R. have mercy on us.

Heart of Jesus, loaded down with opprobrium, R. have mercy on us.

Heart of Jesus, bruised for our offenses, R. have mercy on us.

Heart of Jesus, obedient to death, R. have mercy on us.

Heart of Jesus, pierced with a lance, R. have mercy on us.

Heart of Jesus, source of all consolation, R. have mercy on us.

Heart of Jesus, our life and resurrection, R. have mercy on us.

Heart of Jesus, our peace and reconciliation, R. have mercy on us.

Heart of Jesus, victim for our sins R. have mercy on us.

Heart of Jesus, salvation of those who trust in Thee, R. have mercy on us.

Heart of Jesus, hope of those who die in Thee, R. have mercy on us.

Heart of Jesus, delight of all the Saints, R. have mercy on us.

Lamb of God, who takest away the sins of the world, R. spare us, O Lord.

Lamb of God, who takest away the sins of the world, R. graciously hear us, O Lord.

Lamb of God, who takest away the sins of the world, R. have mercy on us, O Lord.

V. Jesus, meek and humble of heart. R. Make our hearts like to Thine.

Let us pray;

Almighty and eternal God, look upon the Heart of Thy most beloved Son and upon the praises and satisfaction which He offereth Thee in the name of sinners; and to those who implore Thy mercy, in Thy great goodness, grant forgiveness in the name of the same Jesus Christ, Thy Son, who liveth and reigneth with Thee forever and ever. R. Amen.

