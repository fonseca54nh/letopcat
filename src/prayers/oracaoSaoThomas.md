# Oração de São Thomás de Aquino

Criador inefável, Tu que és a fonte verdadeira da luz e da ciência, derrama sobre as trevas da minha inteligência um raio da tua claridade. Dá-me inteligência para compreender, memória para reter, facilidade para aprender, subtileza para interpretar, e graça abundante para falar.

Meu Deus, semeia em mim a semente da tua bondade. Faz-me pobre sem ser miserável, humilde sem fingimento, alegre sem superficialidade, sincero sem hipocrisia; que faça o bem sem presunção, que corrija o próximo sem arrogância, que admita a sua correção sem soberba, que a minha palavra e a minha vida sejam coerentes.

Concede-me, Verdade das verdades, inteligência para conhecer-te, diligência para te procurar, sabedoria para te encontrar, uma boa conduta para te agradar, confiança para esperar em ti, constância para fazer a tua vontade.

Orienta, meu Deus, a minha vida, concede-me saber o que tu me pedes e ajuda-me a realizá-lo para o meu próprio bem e de todos os meus irmãos.

Amém.
