[Coroinha de Nossa Senhora](coroinha.md){class="link"}

# Magnificat

Magnifícat 
ánima mea Dóminum

Et exultávit spíritus meus
in Deo salutári meo.

Quia respéxit humilitátem ancíllæ suæ:
Ecce enim ex hoc beátam me dicent omnes generationes.

Quia fecit mihi magna qui potens est:
et sanctum nomen eius.

Et misericórdia eius a progénie in progénies 
timéntibus eum.

Fecit poténtiam in bráchio suo:
dispérsit supérbos mente cordis sui.

Depósuit poténtes de sede, 
et exaltávit húmiles.

Esuriéntes implévit bonis: 
et dívites dimísit inánes.

Suscépit Israel púerum suum,
recordátus misericórdiæ suæ.

Sicut locútus est ad patres nostros, 
Abraham et sémini eius in sáecula

Glória Patri, et Fílio,
et Spirítui Sancto.

Sicut erat in princípio, et nunc, et semper,
et in sáecula saeculórum.
Amen
