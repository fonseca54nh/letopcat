# Novena a Nossa Senhora das Graças

Método de rezar a novena:

1. Ato de contrição;
2. Meditação do dia;
3. Súplica a Nossa Senhora;
4. Três Ave-Marias, depois a jaculatória: “Ó Maria concebida sem pecado, rogai por nós que recorremos a vós!”;
5. Oração final.

## 1 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Primeira aparição

Contemplemos a Virgem Imaculada em sua primeira aparição a Santa Catarina Labouré. A piedosa noviça, guiada por seu Anjo da Guarda, é apresentada à Imaculada Senhora. Consideremos sua inefável alegria. Seremos também felizes como Santa Catarina se trabalharmos com ardor na nossa santificação. Gozaremos as delícias do Paraíso se nos privarmos dos gozos terrenos.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 2 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Lágrimas de Maria

Contemplemos Maria chorando sobre as calamidades que viriam sobre o mundo, pensando que o coração de seu Filho seria ultrajado na cruz, escarnecido, e seus filhos prediletos perseguidos. Confiemos na Virgem compassiva e também participemos do fruto de suas lágrimas.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 3 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação – Proteção de Maria

Contemplemos Nossa Imaculada Mãe, dizendo, em suas aparições, a Santa Catarina: “Eu mesma estarei convosco: não vos perco de vista e vos concederei abundantes graças”. Sede para mim, Virgem Imaculada, o escudo e a defesa em todas as necessidades.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 4 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Segunda aparição

Estando Santa Catarina Labouré em oração, a 27 de novembro de 1830, apareceu-lhe a Virgem Maria, formosíssima, esmagando a cabeça da serpente infernal. Nessa aparição, vemos seu desejo imenso de nos proteger sempre contra o inimigo de nossa salvação. Invoquemos a Imaculada Mãe com confiança e amor.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 5 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - As mãos de Maria

Contemplemos, hoje, Maria desprendendo de suas mãos raios luminosos. “Estes raios, disse Ela, são a figura das graças “que derramo sobre todos aqueles que mas pedem e aos que trazem com fé a minha medalha”. Não desperdicemos tantas graças! Peçamos, com fervor, humildade e perseverança, pois Maria Imaculada nos alcançará.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 6 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Terceira aparição

Contemplemos Maria aparecendo a Santa Catarina, radiante de luz, cheia de bondade, rodeada de estrelas, mandando cunhar uma medalha e prometendo muitas graças a todos que a trouxerem com devoção e amor. Guardemos fervorosamente a Santa Medalha, pois, como um escudo, ela nos protegerá dos perigos.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 7 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Súplica

Ó Virgem Milagrosa, Rainha Excelsa Imaculada Senhora, sede minha advogada, meu refúgio e asilo nesta terra, meu consolo nas tristezas e aflições, minha fortaleza e advogada na hora da morte.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 8 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Súplica

Ó Virgem Imaculada da Medalha Milagrosa, fazei com que esses raios luminosos que irradiam de vossas mãos virginais iluminem minha inteligência para melhor conhecer o bem e abrasem meu coração, vivos sentimentos de fé, esperança e caridade.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

# 9 Dia

### Ato de contrição

Senhor meu, Jesus Cristo, Deus e Homem verdadeiro, Criador e Redentor meu, por serdes Vós quem sois, sumamente bom e digno de ser amado sobre todas as coisas, e porque Vos amo e estimo, pesa-me, Senhor, por Vos ter ofendido e pesa-me também por ter perdido o Céu e merecido o inferno. Proponho, firmemente, com o auxílio de Vossa divina graça e pela poderosa intercessão de Vossa Mãe Santíssima, emendar-me e nunca mais Vos tornar a ofender. Espero alcançar o perdão de minhas culpas, por Vossa infinita misericórdia. Assim seja.

### Meditação - Súplica

Ó Mãe Imaculada, fazei com que a cruz de vossa Medalha brilhe sempre diante de meus olhos, suavize as penas da vida presente e conduza-me à vida eterna.

### Súplica a Nossa Senhora

Ó Imaculada Virgem Mãe de Deus e nossa Mãe, ao contemplar-vos de braços abertos derramando graças sobre os que vo-las pedem, cheios de confiança na vossa poderosa intercessão, inúmeras vezes manifestada pela Medalha Milagrosa, embora reconhecendo a nossa indignidade por causa de nossas inúmeras culpas, acercamo-nos de vossos pés para vos expor, durante essa oração, as nossas mais prementes necessidades (momento de silêncio e de pedir a graça desejada).

Concedei, pois, ó Virgem da Medalha Milagrosa, este favor que confiantes vos solicitamos, para maior glória de Deus, engrandecimento do vosso nome, e o bem de nossas almas. E para melhor servirmos ao Vosso Divino Filho, inspirai-nos profundo ódio ao pecado e dai-nos coragem de nos afirmar sempre verdadeiros cristãos. Amém.

### Três Ave-Marias e Jaculatória

Rezar três Ave-Marias.

Jaculatória: Ó Maria concebida sem pecado, rogai por nós que recorremos a vós.

### Oração final

Santíssima Virgem, eu reconheço e confesso vossa Santa e Imaculada Conceição, pura e sem mancha. Ó puríssima Virgem Maria, por vossa Conceição Imaculada e gloriosa prerrogativa de Mãe de Deus, alcançai-me de vosso amado Filho a humildade, caridade, obediência, castidade, santa pureza de coração, de corpo e espírito; alcançai-me a perseverança na prática do bem, uma santa vida, uma boa morte e a graça de (pede-se uma graça) que peço com toda a confiança. Amém.

