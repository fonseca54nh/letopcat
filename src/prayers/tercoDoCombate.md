<center>
# Terço do Combate( Instituto Hesed )
</center>

### Início

Pelo sinal da Santa Cruz livrai-nos Deus Nosso Senhor, dos nossos inimigos. Em nome do Pai do Filho e do Espírito Santo. Amém.


* Pai Nosso
* Ave Maria
* Creio

### São 5 mistérios, onde em cada um reza-se:

* [Augusta Rainha](augustaRainha.html)
* [Pequeno Exorcismo de São Miguel](miguelLeaoXIII.html)

> Jesus Tu vencerás como sempre Tu venceste, por mais árdua que seja a batalha a vitória será Tua.

> São Miguel e todos os santos anjos, combatei e rogai por nós. x10

### Final 

> São Miguel, São Gabriel, São Rafael, Santo anjo da guarda, combatei e rogai por nós.

[Consagração a São Miguel Arcanjo](consagracaoSaoMiguel.html)
