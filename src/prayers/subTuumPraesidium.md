[Coroinha de Nossa Senhora](coroinha.html){class="link"}

# Sub Tuum Praesidium

Sub Tuum Praesidium confúgimus, Santa Dei génitrix
Nostras Deprecationes ne despicias in nececitatibus
Sed a periculis cunctis
Libera nos semper
Virgo gloriosa et benedicta
