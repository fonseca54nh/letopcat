<center>
# Credo
</center>

Credo in unum Deum, 
Patrem omnipoténtem,
factórem caeli et terrae,
visibílium ómnium, 
et invisibílium.

Et in unum Dóminum
Iesum Christum,
Fílium Dei unigénitum.

Et ex Patre natum 
ante ómnia sáecula.

Deum de Deo, 
lumen de lúmine,
Deum verum de Deo vero.

Génitum, non factum, 
consubstantiálem Patri:
per quem ómnia facta sunt.

Qui propter nos hómines 
et propter nostram salútem
descendit de caelis.

Et incarnátus est de Spíritu Sancto
ex María Vírgine:
Et homo factus est.

Crucifíxus étiam pro nobis:
sub Póntio Piláto
passus, et sepúltus est.

Et resurréxit tértia die,
secúndum Scriptúras.

et ascéndit in caelum:
sedet ad déxteram Patris.

Et íterum vetúrus est cum glória, 
iudicáre vivos et mórtuos:
cuius regni non erit finis.

Et in Spíritum Sanctum, Dóminum, 
et vivificántem: 
qui ex Patre Filióque procédit.

Qui cum Patre et Fílio 
simul adorátur
et conglorificátur: 
qui locútus est per Prophétas.

Et unam sanctam cathólicam 
et apostólicam Ecclésiam.

Confíteor unum basptísma 
in remissiónem peccatorum.

Et exspécto resurrectionem mortuorum.

Et vitam ventúri sáeculi. Amem.
