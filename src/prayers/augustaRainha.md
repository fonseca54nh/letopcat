[Terço Do Combate](tercoDoCombate.html){class="link"}

# Augusta Rainha


## Original in French

Auguste Reine des cieux, souveraine maîtresse des Anges,
Vous qui, dès le commencement, avez reçu de Dieu
le pouvoir et la mission d'écraser la tête de Satan,
Nous vous le demandons humblement,
Envoyez vos légions célestes pour que,
sous vos ordres, et par votre puissance,
Elles poursuivent les démons, les combattent partout,
Répriment leur audace, et les refoulent dans l'abîme.
Qui est comme Dieu?
O bonne et tendre mère,
Vous serez toujours notre Amour et notre espérance.
O Divine Mère,
Envoyez les Saints Anges pour nous défendre,
Et repoussez loin de nous le cruel ennemi.
Saints Anges et Archanges,
Défendez nous, gardez nous.

## Portuguese

Augusta Rainha dos céus, soberana mestra dos Anjos,
Vós que, desde o princípio, recebestes de Deus
o poder e a missão de esmagar a cabeça de Satanás,
Nós vo-lo pedimos humildemente,
Enviai vossas legiões celestes para que,
sob vossas ordens, e por vosso poder,
Elas persigam os demônios, combatendo-os por toda a parte,
Reprimindo-lhes a insolência, e lançando-os no abismo.
Quem é como Deus?
Ó Mãe de bondade e ternura,
Vós sereis sempre o nosso Amor e a nossa esperança.
Ó Mãe Divina,
Enviai os Santos Anjos para nos defenderem,
E repeli para longe de nós o cruel inimigo.
Santos Anjos e Arcanjos,
Defendei-nos e guardai-nos. Amém.
