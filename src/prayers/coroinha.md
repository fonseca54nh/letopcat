<center>
# Coroinha de Nossa Senhora
</center>

[Signum Crucis](signumCrucis.html)

> Dignare me laudare te, Virgo sacrata, da mihi virtutem contra hostes tuos.

[Credo](credo.html)

1) Coroa de Excelência

[Pai Nosso](paterNoster.html)

[Ave Maria](aveMaria.html)

Sois feliz, Virgem Maria, que trouxestes, em vosso seio, o Senhor, Criador do mundo; destes à luz a quem te criou, e sois Virgem Perpétua.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes. 

[Ave Maria](aveMaria.html)

Ó sagrada e imaculada virgindade, não sei com que louvores vos possa enaltecer; pois quem os céus não podem conter, vós O levastes em Vosso seio.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Sois toda bela, ó Virgem Puríssima, e não há pecado em vós.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Possuís, ó Virgem Mãe, tantos privilégios, quantas são as estrelas no céu.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Glória ao Pai](gloriaPater.html)

2) Coroa de Poder

[Pai Nosso](paterNoster.html)

[Ave Maria](aveMaria.html)

Glória a vós, Rainha do céu, conduzi-nos convosco aos gozos do paraíso.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, ó Dispensadora das graças do Senhor, dai-nos parte em vosso tesouro.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, Medianeira entre Deus e os homens, tornai-nos favorável o Todo-poderoso.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, que venceis as heresias e o demônio: sede nossa bondosa guia.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Glória ao Pai](gloriaPater.html)

3) Coroa de Bondade

[Pai Nosso](paterNoster.html)

[Ave Maria](aveMaria.html)

Glória a vós, amparo dos pecadores; intercedei por nós junto ao Senhor.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, Mãe dos órfãos; fazei que nos seja favorável o Pai Todo-poderoso.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, alegria dos justos; conduzi-nos convosco à felicidade do céu.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Ave Maria](aveMaria.html)

Glória a vós, nossa Auxiliadora mui cuidadosa na vida e na morte; conduzi-nos convosco para o Reino do Céu.

V. Alegrai-vos, Virgem Maria.

R. Alegrai-vos mil vezes.

[Glória ao Pai](gloriaPater.html)

**Oremos:**

	Ave, Maria, Filha de Deus Pai; Ave, Maria, Mãe de Deus Filho; Ave, Maria, Esposa do Espírito Santo; Ave, Maria, Templo da Santíssima Trindade; Ave, Maria, Senhora minha, meu bem, meu amor, Rainha do meu coração, Mãe, vida, doçura e esperança minha mui querida, meu coração e minha alma. Sou todo vosso, e tudo o que possuo é vosso, ó Virgem sobre todas bendita. Esteja, pois, em mim a vossa alma para engrandecer o Senhor; esteja em mim o vosso espírito para rejubilar em Deus. Colocai-vos, ó Virgem fiel, como selo sobre o meu coração, para que, em vós e por vós, seja eu achado fiel a Deus. Concedei, ó Mãe de misericórdia, que me encontre no número dos que amais, ensinais, guiais, sustentais e protegeis como filhos. Fazei que, por vosso amor, despreze todas as consolações da terra e aspire só às celestes; até que, para glória do Pai, Jesus Cristo, Vosso Filho, seja formado em mim, pelo Espírito Santo, vosso Esposo, fidelíssimo, e por vós, sua Esposa mui fiel. Amém.
	
[Sub Tuum Praesidium](subTuumPraesidium.html)

[Magnificat](magnificat.html)
