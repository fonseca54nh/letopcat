<center>
# Coroa ou Rosário de Nossa Senhora das Lágrimas
</center>

###  Oração inicial:
	- Eis-nos aqui aos Vossos pés, ó dulcíssimo Jesus Crucificado, para Vos oferecermos as lágrimas d’Aquela que, com tanto amor, Vos acompanhou no caminho doloroso do Calvário. Fazei, ó bom Mestre, que nós saibamos aproveitar da lição que elas nos dão, para que, na Terra, realizando a Vossa Santíssima Vontade, possamos um dia, no Céu, Vos louvar por toda a eternidade.

### Nas contas grandes:
  - Vêde, ó Jesus, que são as lágrimas d’Aquela que mais Vos amou na Terra, e que mais Vos ama no Céu.
 
### Nas contas pequenas(grupos de 7 contas):
	- Meu Jesus, ouvi os nossos rogos, pelas Lágrimas de Vossa Mãe Santíssima.

###  No fim, repete-se três vezes: 
  - Vêde, ó Jesus, que são as lágrimas d’Aquela que mais Vos amou na Terra, e que mais Vos ama no Céu.

### Oração final:
 - Virgem Santíssima e Mãe das Dores, nós Vos pedimos que junteis os Vossos rogos aos nossos, a fim de que Jesus, Vosso Divino Filho, a quem nos dirigimos em nome das Vossas lágrimas de Mãe, ouça as nossas preces e nos conceda, com as graças que desejamos, a coroa da vida eterna. Amém.

### Jaculatórias finais (para rezar contemplando e beijando a medalha):
	– Por Vossa mansidão divina, ó Jesus Manietado, salvai o mundo do erro que o ameaça!
	– Ó Virgem Dolorosíssima, as Vossas Lágrimas derrubaram o império infernal!
