# Prayers

[Angelus Dei](angelusDei.html){class="link"}

[Angelus Domini](angelusDomini.html){class="link"}

[Augusta Rainha](augustaRainha.html){class="link"}

[Consagração a São Miguel](consagracaoSaoMiguel.html){class="link"}

[Coroa Das Lágrimas](coroaDasLagrimas.html){class="link"}

[Coroinha de Nossa Senhora](coroinha.html){class="link"}

[Credo](credo.html){class="link"}

[Gloria Pater](gloriaPater.html){class="link"}

[Ladainha do Sagrado Coração De Jesus](ladainhaSagradoCoracaoDeJesus.html){class="link"}

[Magnificat](magnificat.html){class="link"}

[Pequeno Exorcismo de São Miguel Leão XIII](miguelLeaoXIII.html){class="link"}

[Novena Nossa Senhora das Graças](novNossaSenGracas.html){class="link"}

[Oração de São Thomas de Aquino](oracaoSaoThomas.html){class="link"}

[Signum Crucis](signumCrucis.html){class="link"}

[Sub Tuum Praesidium](subTuumPraesidium.html){class="link"}

[Terço do Combate(Insituto Hesed)](tercoDoCombate.html){class="link"}

