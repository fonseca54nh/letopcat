# About Me

Hi there, I'm Bruno, aka Topcat, welcome to my domain. Here you can find more about me, my projects, and also browse interesting stuff I share. I would be grateful if you, dear visitor, would find anything that would enlighten your mind and, perhaps, help you somehow. You'll find out that I like to talk about different topics. From religion to music, passing through computing, I seek knowledge that help us, humans, to develop our souls, and hopefully reach Christ someday.

**May the Virgin Mary guide your tour through my blog and bless you.**

# Religion

I am a devout catholic, and of course, I would include a [prayers](construction.html){class="link"} section. There you can find some prayers I've collected throughout the years. Hopefully, you'll find something there, that helps your soul achieve Christ.

# Work

[Here](construction.html){class="link"} you can find my professional information in detail, but for those who aren't curious enough to read, I'm a computer engineer mostly interested in developing the Linux Kernel. I advocate for the use of open-source source, and seek to help its development.

# Music

For as long as I've known myself, I've always loved music, so I'll try to include some posts and good music I find at the [music](music.html){class="link"} section.

# Blog

[Here](posts.html){class="link"} I keep some posts with wildly varying topics, about life, professional experience and any other content I find useful.

# Contact info

[Mail](mailto:bfonseca@ipt.br){class="link"}

[LinkedIn](https://www.linkedin.com/in/bruno-gabriel-da-fonseca-2290261ba/){class="link"}
