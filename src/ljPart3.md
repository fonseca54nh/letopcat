# A new chapter - Diving even deeper into the kernel

Well, well, well, it seems, it is not possible for a man to a have a single moment of peace. And I, accordingly, started my masters course, so I could throw away my miserable and tiny moments of peace. However, a door to diving even deeper into kernel development was opened. This time, I was taking a discipline - that was actually ministered by my advisor - called Open-Source Software Development (pretty suggestive name). 

The plot was to send a patch to the Linux kernel, it could be as simple as a spelling mistake, but we were forced to send anything that would follow the kernel contributing guide. Well, I was already professionally working with the Linux kernel, so I thought this ought to be easy, right? You Fool! Let me enlighten your mind about how hard it is to actually contribute something useful for the kernel. 

Back at work, we had a bug with a Lenovo Ideapad machine. The problem was that, when we pressed some of the <Fn> keys, the machine would suddenly turn off. Well, there was a driver called ideapad-laptop, when we started to study and debug this driver to find what was causing that.
However, somebody was able to fix the problem before us, and I was empty-handed, and had no patch to present for the discipline. 

Well I had to find anything that would fit, and the other disciplines were trying to bite me, so, yes, I've sent a spelling mistake patch 🙃. I know, for somebody that was already working with the Kernel, this is rather too simple. But in the end, I just couldn't figure out anything better. 

Creating a simple patch like this was easy. At every kernel tree you download, there is a scripts folder, with a bunch of nice stuff in there. One of them is the checkpath.pl script. This script checks source code, checking spelling and cosmetic mistakes. With this, you can find easy stuff to fix in the kernel. Almost like free-real-state.

Creating a patch was easy, thanks to git. In a nutshell, just create a new branch, make your changes, commit, generate the patch, check your path with checkpath.pl(yes here it is again!), sign-off, and send it to maintainers. Seems simple. Is Hard.

After all these steps, this was the final patch that was sent:

```
Date: Tue,  9 Apr 2024 08:40:45 -0300
From: topcat <bfonseca@ime.usp.br>
Cc: topcat <bfonseca@ipt.br>, ac100@lists.launchpad.net,  linux-tegra@vger.kernel.org, linux-staging@lists.linux.dev, linux-kernel@vger.kernel.org
Subject: [PATCH] Fixed spelling mistake
X-Mailer: git-send-email 2.43.0

---
 drivers/staging/nvec/nvec.c | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/drivers/staging/nvec/nvec.c b/drivers/staging/nvec/nvec.c
index 282a664c9176..b4485b10beb8 100644
--- a/drivers/staging/nvec/nvec.c
+++ b/drivers/staging/nvec/nvec.c
@@ -712,7 +712,7 @@ static irqreturn_t nvec_interrupt(int irq, void *dev)
         * TODO: replace the udelay with a read back after each writel above
         * in order to work around a hardware issue, see i2c-tegra.c
         *
-        * Unfortunately, this change causes an intialisation issue with the
+        * Unfortunately, this change causes an initialisation issue with the
         * touchpad, which needs to be fixed first.
         */
	udelay(100);
--                                                                                                                                                                                                          
2.43.0     
```

A much harder problem was to properly configure git send-email to send the patch. And this was the hardest part, believe me when I say. Why, you may ask? Well, because mail providers are stupid. I was trying to use my enterprise email (OUTLOOK), because I was able to configure it with Neomutt and thought it would be easy to configure with git send-email, just to find out - after hours of debugging - that I was not allowed to use less secure apps, and couldn't do anything about, while thinking that I was dumb and couldn't setup SMTP.

Well, in the end I've used another email provider (GMAIL), and was finally able to configure SMTP. For those who are suffering to configure git send-email with SMTP, this is the config that I've used:

```
[user]
	email = yourEmail@provider.com
	name = "Your Beautiful Name"
[sendemail.linux]
	tocmd ="`pwd`/scripts/get_maintainer.pl --nogit --nogit-fallback --norolestats --nol"
	cccmd ="`pwd`/scripts/get_maintainer.pl --nogit --nogit-fallback --norolestats --nom"
	smtpEncryption = tls
	smtpServer = smtp.gmail.com
	smtpUser = yourEmail@provider.com
	smtpServerPort = 587
```


In the end, these were the answers I've had after sending the patch:

```
Date: Tue, 9 Apr 2024 15:15:48 +0300
From: Dan Carpenter <dan.carpenter@linaro.org>
Cc: Marc Dietrich <marvin24@gmx.de>,  Greg Kroah-Hartman <gregkh@linuxfoundation.org>, topcat <bfonseca@ipt.br>, ac100@lists.launchpad.net,  linux-tegra@vger.kernel.org, linux-staging@lists.linux.dev, linux-kernel@vger.kernel.org                                                                                                                                                                        
Subject: Re: [PATCH] Fixed spelling mistake                                                                                                                                                                 

1) Fix your From header.  Use your real name like signing a legal
   document.
2) Use the subsystem prefix in the subject
3) Add a commit message
4) Add a Signed-off-by-line
5) Run scripts/checkpatch.pl on your patch

https://staticthinking.wordpress.com/2022/07/27/how-to-send-a-v2-patch/

regards,
dan carpenter

```

```
Date: Tue, 9 Apr 2024 15:22:38 +0200
From: Nam Cao <namcao@linutronix.de>
Cc: Marc Dietrich <marvin24@gmx.de>,  Greg Kroah-Hartman <gregkh@linuxfoundation.org>, topcat <bfonseca@ipt.br>, ac100@lists.launchpad.net,  linux-tegra@vger.kernel.org, linux-staging@lists.linux.dev, linux-kernel@vger.kernel.org                                                                                                                                                                        
Subject: Re: [PATCH] Fixed spelling mistake                                                                                                                                                                 

On 09/Apr/2024 topcat wrote:
> ---                                                                                                                                                                                                       
>  drivers/staging/nvec/nvec.c | 2 +-                                                                                                                                                                       
>  1 file changed, 1 insertion(+), 1 deletion(-)                                                                                                                                                            
>                                                                                                                                                                                                           
> diff --git a/drivers/staging/nvec/nvec.c b/drivers/staging/nvec/nvec.c                                                                                                                                    
> index 282a664c9176..b4485b10beb8 100644                                                                                                                                                                   
> --- a/drivers/staging/nvec/nvec.c                                                                                                                                                                         
> +++ b/drivers/staging/nvec/nvec.c                                                                                                                                                                         
> @@ -712,7 +712,7 @@ static irqreturn_t nvec_interrupt(int irq, void *dev)                                                                                                                                 
>        * TODO: replace the udelay with a read back after each writel above                                                                                                                                
>        * in order to work around a hardware issue, see i2c-tegra.c                                                                                                                                        
>        *                                                                                                                                                                                                  
> -      * Unfortunately, this change causes an intialisation issue with the                                                                                                                                
> +      * Unfortunately, this change causes an initialisation issue with the                                                                                                                               
>        * touchpad, which needs to be fixed first.                                                                                                                                                         
>        */                                                                                                                                                                                                 
>       udelay(100);                                                                                                                                                                                        

A patch has already been sent to fix the exact same problem:
https://lore.kernel.org/linux-staging/20240331170548.81409-1-dorine.a.tipo@gmail.com/T/#u

Best regards,
Nam
        
```

Sadly somebody had sent the patch before me, again.😵‍💫, but at least I was able to successfully send a patch for the Linux Kernel, despite all the usual minor difficulties that arose.
