Here you can find some music I wrote and/or recorded. Hope You like it.

# Salve Rainha Mãe de Deus - Cover

I don't really know the author of this song, but I've rearranged it to my linking. This arrangement contains a string quartet, with french horns and organ alognside the vocals.

<audio controls src="salveRainhaMaeDeDeus.flac" preload="auto" name="salveRainhaMaeDeDeus" id="salveRainhaMaeDeDeus"></audio>

# Cor dulce, Cor amabile

It's a traditional polyphonic chant praising Jesus's Sacred Heart.

<audio controls src="Cor Jesu, Cor amabile.wav" preload="auto" name="corDulceCorAmabile" id="corDulceCorAmabile"></audio>
