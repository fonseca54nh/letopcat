
# A new foe - Kw

This time a new foe arose in my path to achieve Linux. Kworkflow(kw for the intimate) is a project that tries to automate the kernel build process. As a natural follow up to the previous contribution to the Linux kernel, this contribution seems a logic step, specially being a necessary step for my master's course FOSS discipline.

After some time looking some issues to contribute, I've came across this [one](https://github.com/kworkflow/kworkflow/issues/1095). It required to implement two functions: one to fetch and the other to decode EDID files from sysfs. This was quite simple, as I've already had a script to do such thing. However, upon further consideration, I've came to the conclusion that, since my script used xrandr, it was not suitable for wayland users. So after some rework I've came to a solution to fetch EDID binary files for all connected monitors using sysfs entries.

You can see the [MR](https://github.com/kworkflow/kworkflow/pull/1111/commits) here.
